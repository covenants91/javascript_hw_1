var x = 6;
var y = 14;
var z = 4;
x += y - x++ * z;
result = x;
document.write("x += y - x++ * z = " + result + "<br />");
/*
x += y  це те ж саме, що стверджувати, що x = x + y
x++ в даному прикладі постфіксний інкремент не застосовується
6 + 14 - 6 * 4 = 20 - 24 = -4
*/

var x = 6;
var y = 14;
var z = 4;
z = --x - y * 5;
result = z;
document.write("z = --x - y * 5 = " + result + "<br />");
/*
--x в даному прикладі префіксний декремент застосовується
z = 5 - 14 * 5 = 5 - 70 = -65
*/

var x = 6;
var y = 14;
var z = 4;
y /= x + 5 % z;
result = y;
document.write("y /= x + 5 % z = " + result + "<br />")
/*
y /= x  це те ж саме, що стверджувати, що y = y / x
y /= 14 / 6 + 5 % 4 = 14 / (6 + 1) = 2
*/

var x = 6;
var y = 14;
var z = 4;
z - x++ + y * 5;
result = z - x++ + y * 5;
document.write("z - x++ + y * 5 = " + result + "<br />")
/*
x++ в даному прикладі постфіксний інкремент застосовується
4 - 7 + 14 * 5 = -3 + 70 = 67
*/

var x = 6;
var y = 14;
var z = 4;
x = y - x++ * z;
result = x;
document.write("x = y - x++ * z = " + result + "<br />")
/*
x++ в даному прикладі постфіксний інкремент не застосовується
x = 14 - 6 * 4 = 14 -24 = -10
*/